import utfpr.ct.dainf.if62c.pratica.Time;
import utfpr.ct.dainf.if62c.pratica.Jogador;
/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica61 {
    public static void main(String[] args) {
        Time time1 = new Time();
        Time time2 = new Time();
        
        time1.addJogador("Goleiro", new Jogador(1, "Tafarel"));
        time1.addJogador("Meio", new Jogador(8, "Pato"));
        time1.addJogador("Atacante", new Jogador(11, "Dibraldinho"));

        time2.addJogador("Goleiro", new Jogador(1, "Ceni"));
        time2.addJogador("Meio", new Jogador(7, "Pirlo"));
        time2.addJogador("Atacante", new Jogador(9, "Kaka"));

        time1.compara(time2);
    }
}
